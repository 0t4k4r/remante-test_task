<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ProductsController extends Controller
{
    /**
    * @Route("/products/viewproducts/{product_type}", name="view_products")
    */
    public function viewProductsAction($product_type = "no-products")
    {
        $products_formatted_and_filtered = $this->getProducts($product_type);
        return $this->render('products/viewproducts.html.twig', array(
            "products_formatted_and_filtered" => $products_formatted_and_filtered,
        ));
    }

    private function getProducts($product_type)
    {
      $product_types = array("135-vstrikovaci-cerpadla","141-vysokotlaka-cerpadla","158-vstrikovace");
      if( in_array($product_type, $product_types) ){
        $product_types = array($product_type);
      }elseif($product_type != "all"){
        $product_types = array();
      }
      $all_products_formatted = array();
      foreach($product_types as $product_type){
        //$first_page = file_get_contents('https://www.autonorma.cz/cs/'.$product_type);
        $first_page = file_get_contents('AutoNorma-Sample.html');
        $crawler = new Crawler($first_page);
        $pagination = $crawler->filter('.pagination')->children()->eq(1);
        $page_count = 0;
        foreach($pagination->children() as $page){
          $page_count++;
        }
        $page_count-=2;
        for($page = 1; $page <= $page_count; $page++){
          //$html = file_get_contents('https://www.autonorma.cz/cs/'.$product_type.'?p='.$page);
          $html = file_get_contents('AutoNorma-Sample.html');
          $crawler = new Crawler($html);
          $product_list = $crawler->filter('.product_list');
          $number_of_products = 0;
          foreach ($product_list->children() as $child){
            $number_of_products++;
          }
          for($i = 0; $i < $number_of_products; $i++){
            $product = $product_list->children()->eq($i);
            $product = $product->children()->eq(0);
            $product = $product->children()->eq(1);
            $name = $product->children()->eq(0)->text();
            $pieces = explode(' ', $name);
            $code = array_pop($pieces);
            $code = array_pop($pieces);
            $code = array_pop($pieces);
            $name = implode(' ',$pieces);
            $price = $product->children()->eq(2)->text();
            $price = $product->filter('.content_price')->text();
            $price = explode("Kč",$price);
            $price = $price[0]." Kč";
            array_push($all_products_formatted, array($name, $code, $price));
          }
        }
      }

      return $all_products_formatted;
    }

    /**
    * @Route("/products/exportproductstocsv")
    */
    public function exportToCSVAction()
    {
      $table_content = json_decode($_POST['table_contents_stringified'],true);
      $number_of_products = $_POST['table_rows_count'];
      $response = new Response();
      /*$response->setCallback(function() {
        $file = fopen('php://output', 'w');
        for($i = 0; $i < $number_of_products; $i++){
          fputcsv($file, $table_content[$i], ';');
        }
        fclose($file);
      });*/
      $output = "";
      for($i = 0; $i < $number_of_products; $i++){
        $line = implode(';',$table_content[$i]);
        $output .= $line."\n";
      }
      $response->setContent($output);
      $response->headers->set('Content-Type', 'text/csv; charset="utf-8"');
      $response->headers->set('Content-Disposition', 'attachment; filename=example.csv');
      $response->send();
      return $response;
    }

    /*private function getFilteredProducts($type = "", $brand = "")
    {
      $all_products = $this->getAllProducts($type);
      $filtered_products = [];
      if($brand){
        foreach($all_products as $product){
          if(strpos($product[0],$brand))
            array_push($filtered_products, $product);
        }
      }else{
        $filtered_products = $all_products;
      }
      return $filtered_products;
    }*/

}
